module.exports = {
    roots: ['<rootDir>'],
    transform: {
        '^.+\\.ts?$': 'ts-jest'
    },
    testRegex: '(test|spec)\\.ts?$',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    preset: 'ts-jest',
    globals: {
        'ts-jest': {
            tsConfig: {
                noUnusedLocals: false
            }
        }
    },
    collectCoverageFrom: ['**/*.ts'],
    modulePathIgnorePatterns: [
        './index.ts',
        'src/util/logger.ts',
        // 'src/repositories/types.ts',
        // 'src/services/ioc.ts',
        // 'src/services/types.ts'
    ]
};
  