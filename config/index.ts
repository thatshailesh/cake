export const apiUrl =  process.env.API_URL || 'https://api.coincap.io/v2';
export const rates = {
    'BTC': 3825.281112,
    'ETH': 138.8911,
    'DOGE': 0.00198422341298374987
};