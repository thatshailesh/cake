# Token Service

A simple CLI app built in **typescript** to quickly determine the amount of SALE token a given amount of ETH would fetch. ETHSALE rate is the rate of amount of SALE that 1 ETH can get and is provided as an input text file with the app.

### Prerequisites

```
Node >= v12.18.0
Typescript >= 3.9.3
Jest - Testing framework
Logger - Logging library
Eslint - Linting tool
```

## Folder Structure
```
__fixtures__       # Mock Fixtures
__tests__          # Test cases
src
│   index.ts         # App entry point
└───config          # Environment variables and configuration related stuff
└───services        # All the business logic is here
└───util           # Util libraries such as Logger
```


### Installing

```
clone repo
cd && npm install
npm install jest tsc -g
```

### Start

**Note**: App uses `test.txt` file present in root folder as an input text file

```
npm run build
npm start
```

### Tests

```
npm run test
```

#### Test Coverage
```
 PASS  __tests__/ApiService.test.ts
 PASS  __tests__/TokenService.test.ts
-----------------------------|---------|----------|---------|---------|-------------------
File                         | % Stmts | % Branch | % Funcs | % Lines | Uncovered-Lines 
-----------------------------|---------|----------|---------|---------|-------------------
All files                    |     100 |      100 |     100 |     100 |                   
 __fixtures__                |     100 |      100 |     100 |     100 |                   
  ApiData.ts                 |     100 |      100 |     100 |     100 |                   
 config                      |     100 |      100 |     100 |     100 |                   
  index.ts                   |     100 |      100 |     100 |     100 |                   
 src/services/implementation |     100 |      100 |     100 |     100 |                   
  ApiService.ts              |     100 |      100 |     100 |     100 |                   
  TokenService.ts            |     100 |      100 |     100 |     100 |                   
-----------------------------|---------|----------|---------|---------|-------------------
```

## Author

- **Shailesh Shekhawat** - _Initial work_ - [Github](https://gitlab.com/thatshailesh)

## License

This project is licensed under the MIT License