'use strict';
import readline from 'readline';

import logger from './src/util/logger';
import TokenService from './src/services/implementation/TokenService';

async function processLineByLine() {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    for await (const line of rl) {
        const tokenServiceInstace = new TokenService();
        const lineSplit = line.split(' ');
        if (lineSplit.length === 4) {
            const inpObj = {
                saleRate: lineSplit[0],
                decimalPlaces: lineSplit[1],
                currency: lineSplit[2],
                amount: lineSplit[3],
            };
            const result = await tokenServiceInstace.getSaleToken(inpObj);
            logger.info('✌️ %o', result);
        }
    }
}

try {
    processLineByLine();
}catch(err) {
    logger.error('🔥 Error while starting app: %o', err);
}