export const liveRateByIdResponse = {
    data:{
        id:'bitcoin',
        symbol:'BTC',
        currencySymbol:'₿',
        type:'crypto',
        rateUsd:'9709.5131461576757441'
    },
    timestamp:1591367512717
};

export const BTCUSD = '9709.5131461576757441';

export const DOGEUSD = '0.0025778846339110';