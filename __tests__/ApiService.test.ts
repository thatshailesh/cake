import ApiService from '../src/services/implementation/ApiService';

import { IApiService } from '../src/services/IApiService';

describe('ApiService', () => {
    let apiServiceInstance: IApiService;
    let getLiveRateById: jest.SpyInstance<any>;

    beforeEach(() => {
        apiServiceInstance = new ApiService();
        getLiveRateById = jest.spyOn(apiServiceInstance, 'getLiveRateById');
    });

    it('should be able to get live rate by id', async () => {
        const result = await apiServiceInstance.getLiveRateById('bitcoin');
        expect(result).not.toBeNull();
        expect(getLiveRateById).toBeCalledTimes(1);
        expect(apiServiceInstance.getLiveRateById).toHaveBeenCalledWith('bitcoin');
    });
});