import TokenService from '../src/services/implementation/TokenService';

import { BTCUSD, DOGEUSD } from '../__fixtures__/ApiData';
import { ITokenService } from '../src/services/ITokenService';
import BigNumber from 'bignumber.js';


describe('TokenService', () => {
    
    let tokenServiceInstance: ITokenService;
    const mockLiveRateById: jest.Mock<any> = jest.fn().mockResolvedValue({
        data: {
            rateUsd: BTCUSD
        }
    });

    jest.mock('../src/services/implementation/ApiService', () => ({
        getLiveRateById: mockLiveRateById
    }));
    beforeEach(() => {
        jest.clearAllMocks();
        tokenServiceInstance = new TokenService();
    });

    it('should be able to mock live rate', async () => {
        const getUSDRateById = jest.spyOn(tokenServiceInstance, 'getUSDRateById');
        
        const result = await tokenServiceInstance.getUSDRateById('bitcoin');
        expect(result).not.toBeNull();
        expect(getUSDRateById).toHaveBeenCalledTimes(1);
        mockLiveRateById.mockReturnValueOnce(BTCUSD);
    });
    

    it('should be able to convert 3 BTC to ETH', async () => {
        tokenServiceInstance.getUSDRateById = jest.fn().mockResolvedValue(BTCUSD);
        const currencyInUSD = new BigNumber(await tokenServiceInstance.getUSDRateById('bitcoin')).multipliedBy(3).toFixed();
        const result = await tokenServiceInstance.getETH(currencyInUSD);
        expect(result).not.toBeNull();
        expect(tokenServiceInstance.getUSDRateById).toBeCalledTimes(2);
        expect(tokenServiceInstance.getUSDRateById).toHaveBeenCalledWith('bitcoin');
        expect(tokenServiceInstance.getUSDRateById).toHaveBeenCalledWith('ethereum');
    });

    it('should be able to convert 3 DOGE to ETH', async () => {
        tokenServiceInstance.getUSDRateById = jest.fn().mockResolvedValue(DOGEUSD);
        const currencyInUSD = new BigNumber(await tokenServiceInstance.getUSDRateById('dogecoin')).multipliedBy(3).toFixed();
        const result = await tokenServiceInstance.getETH(currencyInUSD);
        expect(result).not.toBeNull();
        expect(tokenServiceInstance.getUSDRateById).toBeCalledTimes(2);
        expect(tokenServiceInstance.getUSDRateById).toHaveBeenCalledWith('dogecoin');
        expect(tokenServiceInstance.getUSDRateById).toHaveBeenCalledWith('ethereum');
    });

    it('should be able to get sale token given sale rate, decimal places, DOGE Currency and amount', async () => {
        const inpObj = {
            saleRate: '3.5',
            decimalPlaces: '5',
            currency: 'DOGE',
            amount: '8.3',
        }; 
        tokenServiceInstance.getUSDRateById = jest.fn().mockResolvedValue(DOGEUSD);

        const result = await tokenServiceInstance.getSaleToken(inpObj);
        expect(result).not.toBeNull();
        expect(tokenServiceInstance.getUSDRateById).toBeCalledTimes(2);
        expect(tokenServiceInstance.getUSDRateById).toHaveBeenCalledWith('dogecoin');
        expect(tokenServiceInstance.getUSDRateById).toHaveBeenCalledWith('ethereum');
    });

    it('should be able to get sale token given sale rate, decimal places, ETH Currency and amount', async () => {
        const inpObj = {
            saleRate: '3.5',
            decimalPlaces: '5',
            currency: 'ETH',
            amount: '8.3',
        }; 
        const dp = parseInt(inpObj.decimalPlaces, 10);
        const expectedResult = new BigNumber(inpObj.amount).multipliedBy(inpObj.saleRate).toFixed(dp, BigNumber.ROUND_DOWN);
        const result = await tokenServiceInstance.getSaleToken(inpObj);
        expect(result).not.toBeNull();
        expect(result).toBe(expectedResult);
    });

});