import { createLogger, format, transports, Logger } from 'winston';
import path from 'path';

const logDir = 'logs';
const filename: string = path.join(logDir, 'combined.log');

declare const process: {
    env: {
      NODE_ENV: string;
    };
    mainModule: {
        filename: string;
    };
};

const env = process.env.NODE_ENV || 'dev';

const loggerInstance: Logger = createLogger({
    // change level if in dev environment versus production
    level: env === 'prod' ? 'info' : 'debug',
    format: format.combine(
        format.label({ label: path.basename(process.mainModule.filename) }),
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' })
    ),
    transports: [
        new transports.Console({
            format: format.combine(
                format.colorize(),
                format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
                format.splat(),
                format.simple(),
                format.printf((info) => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`)
            ),
        }),
        new transports.File({
            filename,
            level: 'debug',
            format: format.combine(
                format.errors({ stack: true }),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss',
                }),
                format.json()
            ),
        }),
    ],
    exceptionHandlers: [
        new transports.File({ filename: `./${logDir}/exceptions.log` }),
    ],
});

export default loggerInstance;