
export interface IInput {
    saleRate: string; 
    decimalPlaces: string; 
    currency: string; 
    amount: string;
}
export interface ITokenService {
    getUSDRateById(id: string): Promise<string>;
    getETH(value: string): Promise<string>;
    getSaleToken(inp: IInput): Promise<string>;
}