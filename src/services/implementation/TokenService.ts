import { ITokenService, IInput } from '../ITokenService';
import ApiService from './ApiService';
import BigNumber from 'bignumber.js';

const CoinIdMap = new Map([
    ['BTC', 'bitcoin'],
    ['ETH', 'ethereum'],
    ['DOGE','dogecoin']
]);

export default class TokenService implements ITokenService {

    async getUSDRateById(id: string): Promise<string> {
        const apiServiceInstace = new ApiService();
        const { data } = await apiServiceInstace.getLiveRateById(id);

        return data.rateUsd;
    }

    async getSaleToken(inp: IInput): Promise<string> {
        const { saleRate, decimalPlaces, currency, amount } = inp;
        const dp = parseInt(decimalPlaces, 10);
        if (currency !== 'ETH') {
            const currencyRate = await this.getUSDRateById(CoinIdMap.get(currency) as string);
            const totalAmount = new BigNumber(currencyRate).multipliedBy(amount).toFixed();
            const eth = await this.getETH(totalAmount);

            return new BigNumber(eth).multipliedBy(saleRate).toFixed(dp, BigNumber.ROUND_DOWN);
        }

        return new BigNumber(amount).multipliedBy(saleRate).toFixed(dp, BigNumber.ROUND_DOWN);
    }

    async getETH(value: string): Promise<string> {
        const ethRate = await this.getUSDRateById(CoinIdMap.get('ETH') as string);
        const ethPerUSD = new BigNumber(1).dividedBy(ethRate).toFixed();
        const totalETH = new BigNumber(ethPerUSD).multipliedBy(value).toFixed();

        return totalETH;
    }
}