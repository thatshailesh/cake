import fetch from 'node-fetch';
import { IApiService, ApiResponse } from '../IApiService';
import { apiUrl } from '../../../config';

export default class ApiService implements IApiService {
    async getLiveRateById(id: string): Promise<ApiResponse> {
        const url = `${apiUrl}/rates/${id}`;
        const response = await fetch(url);
        return response.json();
    }
}