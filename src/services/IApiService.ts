export interface ApiResponse {
    data: {
        id: string;
        symbol: string;
        currencySymbol: string;
        type: string;
        rateUsd: string;
    };
    timestamp: number;
}
export interface IApiService {
    getLiveRateById(id: string): Promise<ApiResponse>;
}